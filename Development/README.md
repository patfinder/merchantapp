# ANT Shop App

The mobile application which helps to practice and track the amount of sights words a kid can read.

The development process is described in the ["What did I learn" blog](http://whatdidilearn.info/tags#BlitzReading).

## Dev Notes

* When debugging on dev machine, the app seem having problem connecting to port 81. Change to port 80 is ok.

* Run react-native link to link native modules (changes can be committed to git)

react-native link react-native-gesture-handler
react-native link @react-native-community_async-storage
react-native link react-native-vector-icons
react-native link react-native-reanimated

## Example

![App Example](app-example.gif)

## License

[MIT](LICENSE)

# TODO

* App Settings


keytool -genkeypair -v -keystore MerchantApp-key.keystore -alias MerchantAppAlias -keyalg RSA -keysize 2048 -validity 10000

What is your first and last name?
  [Vuong Nguyen]:
What is the name of your organizational unit?
  [SmartDEV]:  Mobile Dev Dept
What is the name of your organization?
  [SmartDEV Company]:
What is the name of your City or Locality?
  [Saigon]:  District 1
What is the name of your State or Province?
  [Saigon]:
What is the two-letter country code for this unit?
  [VN]:
Is CN=Vuong Nguyen, OU=Mobile Dev Dept, O=SmartDEV Company, L=District 1, ST=Saigon, C=VN correct?
  [no]:  yes