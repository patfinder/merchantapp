﻿import React from 'react';
import { Text } from 'react-native';
import { createSwitchNavigator, createAppContainer, } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import Icon5 from 'react-native-vector-icons/FontAwesome5';
import IIcon from 'react-native-vector-icons/Ionicons';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import reducers from './src/reducers';
import i18n from './src/i18n';

import WelcomeScreen from './src/screens/WelcomeScreen';
import ResultsScreen from './src/screens/ResultsScreen';
//import HighScoresScreen from './src/screens/HighScoresScreen';
import LogoutScreen from './src/screens/LogoutScreen';
import SplashScreen from './src/screens/SplashScreen';
import LoginScreen from './src/screens/LoginScreen';
import MainScreen from './src/screens/MainScreen';
import OrderScreen from './src/screens/Order/OrderScreen';
import OrderListScreen from './src/screens/Order/OrderListScreen';
import PreviousOrder  from './src/screens/Order/OrderHistoryScreen';

const MainNavigator = createStackNavigator(
    {
        MainScreen: {
            screen: MainScreen,
            path: '/',
            //headerMode: 'none',
            headerShown: false,
        },
        Order: {
            screen: OrderScreen,
            path: '/order',
            //headerMode: 'float',
            navigationOptions: ({ navigation }) => ({
                title: 'Tạo Đơn Hàng',
            }),
        },
        OrderList: {
            screen: OrderListScreen,
            path: '/order-list',
            navigationOptions: ({ navigation }) => ({
                title: 'Danh Sách Đơn Hàng',
            }),
        },
        PreviousOrder: {
            screen: PreviousOrder,
            path: '/previous-order',
            //headerMode: 'float',
            navigationOptions: ({ navigation }) => ({
                title: 'Đơn hàng trước đây',
            }),
        },
    },
    //{ headerMode: 'none' }
);

const HomeNavigator = createSwitchNavigator({
    //Welcome: WelcomeScreen,
    //Login: LoginScreen,
    Main: MainNavigator,
    Results: ResultsScreen
});

const AppNavigator = createBottomTabNavigator(
    {
        Home: {
            screen: HomeNavigator,
            navigationOptions: {
                //tabBarLabel: ({ tintColor }) => (
                //    <Text style={{ fontSize: 10, color: tintColor }}>
                //        {i18n.t('navigation.home')}
                //    </Text>
                //),
                tabBarIcon: ({ horizontal, tintColor }) =>
                    <Icon5 name="home" size={horizontal ? 20 : 25} color={tintColor} />
            }
        },
        Logout: {
            screen: LogoutScreen, // HighScoresScreen SettingsScreen
            navigationOptions: {
                //tabBarLabel: ({ tintColor }) => (
                //    <Text style={{ fontSize: 10, color: tintColor }}>
                //        {i18n.t('navigation.settings')}
                //    </Text>
                //),
                tabBarIcon: ({ horizontal, tintColor }) => (
                    <IIcon name="md-exit" size={horizontal ? 20 : 25} color={tintColor} />
                ),
                tabBarOnPress: ({ navigation }) => navigation.navigate('Login'),
            }
        }
    },
    {
        tabBarOptions: {
            activeTintColor: 'orange',
            inactiveTintColor: 'gray'
        }
    }
);

const InitialNavigator = createSwitchNavigator({
    Splash: SplashScreen,
    Login: LoginScreen,
    App: AppNavigator
});

const AppContainer = createAppContainer(InitialNavigator);

class App extends React.Component {
    render() {
        return (
            <Provider store={createStore(reducers)}>
                <AppContainer />
            </Provider>
        );
    }
}

export default App;
