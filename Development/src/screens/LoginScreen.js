﻿import React from 'react';
import Imei from 'react-native-imei';
import { connect } from 'react-redux';
import { Keyboard, TextInput, View, ActivityIndicator, Alert, Image, Text, Picker } from 'react-native';
import { Input } from 'react-native-elements';
import { Icon } from 'react-native-vector-icons/FontAwesome';
import RNExitApp from 'react-native-exit-app';

import i18n from '../i18n';
import SETTINGS from '../settings';
import { Button } from '../components/common';

class LoginScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            userName: undefined,
            password: undefined,
            // TOD_: DEBUG
            //userName: 'C-001',
            //userName: 'C30019',
            //password: 'password1',
            imei: undefined,
            serverAddress: undefined,

            loading: false,
        };

        this.servers = {
            'Chọn server': null,
            //DEV:   		'http://10.9.21.199/',
            //DEV:   		'http://192.168.1.59/',
            ANTVN: 		'http://antvn.vn:81/',
            ANTMV: 		'http://antmv.vn:81/',
            ANTLA: 		'http://antla.vn:81/',
            ANTHN: 		'http://anthn.vn:81/',
            ANTTN: 		'http://anttn.vn:81/',
        };

        this.formatImei = this.formatImei.bind(this);
        this.onLogin = this.onLogin.bind(this);
        this.saveLogin = this.saveLogin.bind(this);
        this.onQuit = this.onQuit.bind(this);
    }

    componentDidMount() {
        Imei.getImei().then(imeiList => {
            console.log('imeiList', imeiList);
            if (imeiList ?.length) {
                this.setState({ imei: imeiList[0] });
            }
        });

        this.setState({ serverAddress: 'http://antvn.vn:81/' });
    }

    render() {

        var { loading, userName, password, imei, serverAddress } = this.state;
        var serverKeys = Object.keys(this.servers);

        return (
            <View style={styles.container}>
                <View style={{ height: 70 }}
                    //style={{ textAlign: 'left', justifyContent: ''}}
                >
                    <Image source={require('../../resource/ant2.png')} />
                </View>

                {/* Name & Password */}
                <View style={styles.formRow}>
                    <Input
                        placeholder={i18n.t('login.name_label')}
                        value={userName}
                        onChangeText={userName => this.setState({ userName })}
                        leftIcon={{ type: 'font-awesome', name: 'user' }}
                    />
                </View>
                <View style={styles.formRow}>
                    <Input
                        secureTextEntry
                        placeholder={i18n.t('login.password_label')}
                        inputComponent={TextInput}
                        value={password}
                        onChangeText={password => this.setState({ password })}
                        leftIcon={{ type: 'font-awesome', name: 'lock' }}
                    />
                </View>

                {/* Server List */}
                <View style={styles.formRow}>
                    <Input
                        secureTextEntry
                        inputComponent={Picker}
                        selectedValue={serverAddress}
                        onValueChange={serverAddress => this.setState({ serverAddress })}
                        leftIcon={{ type: 'material', name: 'store' }}
                    >
                        {serverKeys.map(key => (
                            <Picker.Item label={key} value={this.servers[key]} />
                        ))}
                    </Input>
                </View>
                <View style={styles.formRow}>
                    <Text>
                        IMEI: {this.formatImei(imei)}
                    </Text>
                </View>
                <View style={styles.formBottom}>
                    <View style={styles.formButton}><Button onPress={this.onLogin} disabled={loading}>Login</Button></View>
                    <View style={styles.formButton}><Button onPress={this.onQuit}  disabled={loading}>Quit</Button></View>
                </View>

                {/* Loading */}
                {loading &&
                    <View style={styles.loading} pointerEvents='none' >
                        <ActivityIndicator size='large' />
                    </View>
                }
            </View>
        );
    }

    formatImei(imei) {
        if (!imei) return imei;

        // split into 3-digits sections
        if (imei.length % 3) {
            imei = ' '.repeat(3 - (imei.length % 3)) + imei;
        }

        var sections = imei.match(/.{3}/g);
        return sections.join('-').trim();
    }

    onLogin() {

        var { userName, password, imei: Imei, serverAddress } = this.state;

        if (!userName || !password || !serverAddress) {
            Alert.alert('Lỗi', 'Mời bạn nhập Mã KH, Mật khẩu và Chọn Server');
            return;
        }
        var { userName: CustomerNo, password: Password } = this.state;

        this.setState({ loading: true });

        global.loginInfo = { serverAddress };

        fetch(`${SETTINGS.API.API_ROOT}${SETTINGS.API.AUTH.LOGIN}`, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ CustomerNo, Password, Imei })
        })
            .then(resp => {
                return Promise.all([resp, resp.ok && resp.json()])
            })
            .then(([ resp, json ]) => {
                console.log('LoginScreen.onLogin json', [resp, json]);

                this.setState({ loading: false });

                // Success
                if (!resp.ok || json.resultCode !== 'Success') {
                    Alert.alert('Login unsuccess!');
                    return;
                }

                this.saveLogin(resp.headers, json.data);
                this.props.navigation.navigate('Main');
            })
            .catch(error => {
                this.setState({ loading: false });
                Alert.alert('Error', error.message);
            })
            .finally(() => self.setState({ loading: false }));
    }

    saveLogin(headers, data) {

        // None: the below code is not needed because react-native use fetch which handle cookie automatically
        // The only code needed is saving serverAddress

        var { serverAddress } = this.state;

        // Note: this method may be not needed because fetch save credential automatically

        let cookies = headers.get('set-cookie');

        // Get .AspNet.auth-cookie value
        let aspCookie = cookies.split(';').map(c => c.trim()).filter(c => c.indexOf('.AspNet.auth-cookie') === 0);
        if (!aspCookie) throw '.AspNet.auth-cookie not found!';

        // Get cookie value
        let authCookie = aspCookie[0].split('=')[1];

        // Store session token
        let { id, name, role} = data;
        global.loginInfo = { id, name, role, serverAddress, authCookie };
    }

    onQuit() {
        RNExitApp.exitApp();
    }
}

const styles = {
    container: {
        display: 'flex',
        flex: 1,
        //flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    formRow: {
        //flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    caption: {
        width: '35%',
        fontSize: 13,
    },
    input: {
        borderColor: '#CCCCCC',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 50,
        width: '90%',
        //fontSize: 25,
        paddingLeft: 20,
        paddingRight: 20
    },
    formBottom: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        //alignItems, // align children on cross direction
        marginTop: 'auto',
        width: '95%',
        marginBottom: 30
    },
    formButton: {
        //flex: 1,
        //alignSelf: 'flex-end',
        width: '30%',
        //height: '30%',
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
};

//export default LoginScreen;
export default connect()(LoginScreen);
