﻿import React from 'react';
import { connect } from 'react-redux';
import { Keyboard, TextInput, View, ActivityIndicator, Alert, Image, Text, Picker } from 'react-native';
import { Input } from 'react-native-elements';
import { Icon } from 'react-native-vector-icons/FontAwesome';
import RNExitApp from 'react-native-exit-app';

import i18n from '../i18n';
import { Button } from '../components/common';

class LogoutScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        setTimeout(() => this.props.navigation.navigate('Login'), 0);
    }

    render() {
        return (
            <View>
                <View style={styles.formBottom}>
                    {
                        //<View style={styles.formButton}>
                        //    <Button onPress={() => this.props.navigation.navigate('Login')}>Login</Button>
                        //</View>
                    }
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        display: 'flex',
        flex: 1,
        //flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    formRow: {
        //flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    caption: {
        width: '35%',
        fontSize: 13,
    },
    input: {
        borderColor: '#CCCCCC',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 50,
        width: '90%',
        //fontSize: 25,
        paddingLeft: 20,
        paddingRight: 20
    },
    formBottom: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        //alignItems, // align children on cross direction
        marginTop: 'auto',
        width: '95%',
        marginBottom: 30
    },
    formButton: {
        //flex: 1,
        //alignSelf: 'flex-end',
        width: '30%',
        //height: '30%',
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
};

export default connect()(LogoutScreen);
