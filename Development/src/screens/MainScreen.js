﻿import React from 'react';
import { Keyboard, TextInput, View, ActivityIndicator, Alert, Image, Text, Picker } from 'react-native';
import Icon5 from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconA from 'react-native-vector-icons/AntDesign';
import IconE from 'react-native-vector-icons/Entypo';
import i18n from '../i18n';
import { Button } from '../components/common';

class MainScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
        };

        this.onLogin = this.onLogin.bind(this);
        this.showNotImplemented = this.showNotImplemented.bind(this);
    }

    render() {

        var { loading, /*userName, password, inventory, inventoryList*/ } = this.state;

        return (
            <View style={styles.container}>
                <View style={{ height: 70 }}>
                    <Image source={require('../../resource/ant2.png')} />
                </View>
                <View style={styles.formRow}>
                    <View style={styles.formButton}>
                        <Button onPress={() => this.props.navigation.navigate('Order')}>
                            <Icon5 style={styles.buttonIcon} name="cart-plus" size={30} />
                            <Text style={styles.buttonCaption}>Đặt hàng</Text>
                        </Button>
                    </View>
                    <View style={styles.formButton}>
                        <Button onPress={this.showNotImplemented} >
                            <Icon name="dollar" size={30} />
                            <Text style={styles.buttonCaption}>Thanh toán</Text>
                        </Button>
                    </View>
                    <View style={styles.formButton}>
                        <Button onPress={() => this.props.navigation.navigate('PreviousOrder')} >
                            <Icon name="dollar" size={30} />
                            <Text style={styles.buttonCaption}>Đơn hàng cũ</Text>
                        </Button>
                    </View>
                </View>
                {
                    //<View style={styles.formRow}>
                    //    <View style={styles.formButton}>
                    //        <Button /*onPress={() => this.props.navigation.navigate('Order')}*/ >
                    //            <IconE name="text-document" size={30} />
                    //            <Text style={styles.buttonCaption}>Chính sách</Text>
                    //        </Button>
                    //    </View>
                    //    <View style={styles.formButton}>
                    //        <Button /*onPress={() => this.props.navigation.navigate('Order')}*/ >
                    //            <Icon name="history" size={30} />
                    //            <Text style={styles.buttonCaption}>Quản lý ĐH</Text>
                    //        </Button>
                    //    </View>
                    //</View>
                    //<View style={styles.formRow}>
                    //    <View style={styles.formButton}>
                    //        <Button /*onPress={() => this.props.navigation.navigate('Order')}*/ >
                    //            <Icon name="user" size={30} />
                    //            <Text style={styles.buttonCaption}>Quản lý TT</Text>
                    //        </Button>
                    //    </View>
                    //    <View style={styles.formButton}>
                    //        <Button /*onPress={() => this.props.navigation.navigate('Order')}*/ >
                    //            <IconA name="areachart" size={30} />
                    //            <Text style={styles.buttonCaption}>Báo cáo</Text>
                    //        </Button>
                    //    </View>
                    //</View>
                }

                {/* Loading */}
                {loading &&
                    <View style={styles.loading} pointerEvents='none' >
                        <ActivityIndicator size='large' />
                    </View>
                }
            </View>
        );
    }

    onLogin() {
    }

    showNotImplemented() {
        Alert.alert('Thông báo', 'Chức năng chưa được cài đặt');
    }
}

const styles = {
    container: {
        display: 'flex',
        flex: 1,
        //flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    formRow: {
        //flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 5,
        marginBottom: 5,
    },
    caption: {
        width: '35%',
        fontSize: 18,
    },
    input: {
        borderColor: '#CCCCCC',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 50,
        width: '63%',
        fontSize: 25,
        paddingLeft: 20,
        paddingRight: 20
    },
    formBottom: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        //alignItems, // align children on cross direction
        marginTop: 'auto',
        width: '95%',
        marginBottom: 30
    },
    formButton: {
        //flex: 1,
        //alignSelf: 'flex-end',
        width: '40%',
        //height: '30%',
    },
    buttonIcon: {
        //marginLeft: 5,
        //marginRight: 5,
    },
    buttonCaption: {
        paddingLeft: 5,
        fontSize: 18,
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
};

export default MainScreen;
