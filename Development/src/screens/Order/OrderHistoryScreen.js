﻿import React from 'react';
import {
    StyleSheet, View, Text, ScrollView, Picker,
    TouchableOpacity, Keyboard,  ActivityIndicator, TextInput, Alert, Image,  
} from 'react-native';
import moment from 'moment';
import { Dimensions } from "react-native";
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';

//import Icon from 'react-native-vector-icons/Feather';
import i18n from '../../i18n';
import SETTINGS from '../../settings';
import { Button } from '../../components/common';
import { formatVnd } from './Utils';

class OrderHistoryScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            viewMonth: null,
            orders: [],
        };

        const today = new Date();
        const thisMonth = new Date(today.getUTCFullYear(), today.getMonth(), 1);
        const monthRange = [...new Array(18).keys()];
        // [{value, label}]
        this.monthsList = monthRange.map(i => {
            var newMonth = new Date(thisMonth.getTime());
            newMonth.setMonth(newMonth.getMonth() - i);
            return { value: newMonth, label: moment(newMonth).format('MM/YYYY') };
        });

        // Table header
        this.tableHead = [
            'Mã ĐH',
            'Ngày tạo',
            'Tổng KL (Kg)',
            'Tổng Tiền (Đ)',
        ];

        // Column width
        const screenWidth = Math.round(Dimensions.get('window').width);
        var widthPercents = [.20, .25, .20, .30];
        this.columnsWidths = widthPercents.map(p => screenWidth * p);

        // Binding
        this.loadData = this.loadData.bind(this);
    }

    componentDidMount() {
        //this.loadData();
    }

    render() {

        var { loading, viewMonth, orders } = this.state;
        var { monthsList, tableHead, columnsWidths } = this;

        var tableData = orders.map(o => o.arr);

        return (
            <View style={styles.container}>
                <ScrollView style={{ width: '100%' }}>
                    { /*Month combo*/}
                    <View style={styles.formRow}>
                        <Picker
                            selectedValue={viewMonth}
                            style={styles.input}
                            onValueChange={viewMonth => this.loadData(viewMonth)}>

                            <Picker.Item label={i18n.t('order.order_hist_month')} />
                            {monthsList.map(month => (
                                <Picker.Item key={month.value} label={month.label} value={month.value} />
                            ))}
                        </Picker>
                    </View>

                    <Table borderStyle={{ borderWidth: 2, borderColor: '#c8e1ff' }} textStyle={{ textAlign: 'right' }}
                    >
                        <Row data={tableHead} style={styles.head} textStyle={{ ...styles.text, textAlign: 'right' }} widthArr={columnsWidths} />
                        <Rows data={tableData} widthArr={columnsWidths} textStyle={{ textAlign: 'right' }} />
                    </Table>

                </ScrollView>

                {/* Loading */}
                {loading &&
                    <View style={styles.loading} pointerEvents='none' >
                        <ActivityIndicator size='large' />
                    </View>
                }
            </View>
        );
    }

    loadData(viewMonth) {

        this.setState({ loading: true, viewMonth }, doFetch.bind(this));

        function doFetch() {

            var viewMonth2 = moment(viewMonth).format('YYYY-MM-DD');

            fetch(`${SETTINGS.API.API_ROOT}${SETTINGS.API.ORDER.GET_PREVIOUS_ORDERS}?viewmonth=${viewMonth2}`, {
                method: 'GET',
                mode: 'cors',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Cache': 'no-cache'
                },
            })
                .then(resp => {
                    return Promise.all([resp, resp.ok && resp.json()])
                })
                .then(([resp, json]) => {
                    console.log('OrderHistoryScreen - GET_PREVIOUS_ORDERS', json);

                    // Auth error
                    if ([401, 403].indexOf(resp.status) >= 0) {
                        Alert.alert('Thông báo', 'Phiên đang nhập hết hạn. Mời bạn đăng nhập lại.');
                        this.props.navigation.navigate('Login');
                        return;
                    }

                    if (!resp.ok) {
                        Alert.alert('Thông báo', 'Lỗi lấy dữ liệu sản phẩm');
                        return;
                    }

                    var { orders } = json.data;

                    orders.forEach(order => {
                        order.orderDate = moment(order.orderDate).format(SETTINGS.DATE_FORMAT);

                        var { orderNo, orderDate, totalWeight, totalAmount } = order;
                        order.arr = [orderNo, orderDate, totalWeight, formatVnd(totalAmount)];
                    });

                    this.setState({ orders });
                })
                .catch(error => {
                    Alert.alert('Error', error.message);
                })
                .finally(() => this.setState({ loading: false }));
        }
    }
}

const styles = StyleSheet.create({

    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#808B97' },
    text: { margin: 6 },
    row: { flexDirection: 'row', backgroundColor: '#FFF1C1' },
    btn: { width: 58, height: 18, backgroundColor: '#78B7BB', borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' },
    formRow: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    formButtom: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        paddingTop: 7,
        justifyContent: 'center',
    },
    formButton: {
        width: '30%',
        fontSize: 12,
    },
    // Remove button
    listButton: {
        //flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#007aff',
        marginTop: 5,
        marginBottom: 5,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    listBtnText: {
        alignSelf: 'center',
        color: '#007aff',
        //fontSize: 13,
        //fontWeight: '700',
        paddingTop: 13,
        paddingBottom: 13,
        //height: 17,
    },
    listTextBlock: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        borderColor: '#CCCCCC',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 50,
        width: '90%',
        paddingLeft: 20,
        paddingRight: 20
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
});

export default OrderHistoryScreen;
