﻿import React from 'react';
import {
    StyleSheet, View, Text, TouchableOpacity,
    //Keyboard, ScrollView, ActivityIndicator, TextInput, Alert, Image, Picker, 
} from 'react-native';
import { Dimensions } from "react-native";
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';

//import Icon from 'react-native-vector-icons/Feather';
import i18n from '../../i18n';
import settings from '../../settings';
import { Button } from '../../components/common';
import { formatVnd } from './Utils';

class OrderListScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            orders: this.props.navigation.getParam('orders'),
            saveChanges: this.props.navigation.getParam('saveChanges'),
        };

        this.productGroups = this.props.navigation.getParam('productGroups');


        // Test multi rows
        //var orders = [...this.state.orders];
        //for (var i = 0; i < 15; i++) {
        //    orders.push(this.state.orders[0]);
        //}
        //this.state = { ...this.state, orders };

        // Table
        this.tableHead = [
            'Mã',
            'Kg',
            'Bao',
            'Đ.Giá (Đ)',
            'T.Tiền (Đ)',
            'Bỏ',
        ];

        const screenWidth = Math.round(Dimensions.get('window').width);

        // total: 95%
        var widthPercents = [.21, .12, .12, .19, .19, .12];
        // width in pixels
        this.columnsWidths = widthPercents.map(p => screenWidth * p);

        this.footerWidths = [
            this.columnsWidths[0] + this.columnsWidths[1] + this.columnsWidths[2],
            this.columnsWidths[3] + this.columnsWidths[4],
            this.columnsWidths[5]
        ];

        //this.sum1Width = this.columnsWidths[0] + this.columnsWidths[1];
        //this.sum2Width = this.columnsWidths[2] + this.columnsWidths[3] + this.columnsWidths[4];

        this.getWeightAndAmount = this.getWeightAndAmount.bind(this);
        this.removeButton = this.removeButton.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.save = this.save.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    removeButton(rowIndex) {
        return (
            <TouchableOpacity onPress={() => this.removeItem(rowIndex)}>
                <View style={{ ...styles.btn, width: 'auto' }}>
                    <Text style={styles.btnText}>X</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        var { orders } = this.state;
        var tableHead = this.tableHead;
        var { sum1Width, sum2Width } = this;

        // Total weight and Amount
        var total = orders.reduce((acc, cur) => {
            var { weight, amount } = this.getWeightAndAmount(cur);
            return { weight: weight + acc.weight, amount: amount + acc.amount };
        }, { weight: 0, amount: 0 })


        var tableData = orders.map((order, index) => {
            var { productNo, packingType, quantity } = order;
            var { price, amount } = this.getWeightAndAmount(order);

            return [productNo, packingType, quantity, formatVnd(price), formatVnd(amount), this.removeButton(index)];
        });

        //tableData.splice(0, 0, tableHead);

        var footerData = [`Tổng KL: ${total.weight}kg`, `Tổng tiền: ${formatVnd(total.amount)}Đ`, ''];

        return (

            <View style={styles.container}>

                { /* Order Rows */ }
                { /*<ScrollView style={styles.formRow}>*/ }
                <Table borderStyle={{ borderWidth: 2, borderColor: '#c8e1ff' }} textStyle={{ textAlign: 'right' }} 
                >
                    <Row data={tableHead} style={styles.head} textStyle={{ ...styles.text, textAlign: 'right' }} widthArr={this.columnsWidths} />
                    <Rows data={tableData} widthArr={this.columnsWidths} textStyle={{ textAlign: 'right' }} />
                    <Row data={footerData} style={styles.head} textStyle={{ ...styles.text, textAlign: 'right' }} widthArr={this.footerWidths} />
                </Table>
                { /*</ScrollView>*/ }

                <View style={styles.formButtom}>
                    <View style={styles.formButton}><Button onPress={this.save}>Lưu T.Đổi</Button></View>
                    <View style={styles.formButton}><Button onPress={this.cancel}>Quay lại</Button></View>
                </View>
            </View>
        );
    }

    getWeightAndAmount(order) {

        var { productNo, packingType, quantity } = order;

        var product = this.productGroups
            .find(g => g.productNo === productNo)
            .products
            .find(p => p.packingType === packingType);

        var { unitPrice, percent, discount } = product;
        var price = unitPrice - (unitPrice * percent) - discount;

        var weight = packingType * quantity;
        var amount = price * quantity;

        return { weight, price, amount };
    }

    removeItem(idx) {
        var orders = [...this.state.orders];
        orders.splice(idx, 1);
        this.setState({ orders });
    }

    save() {
        this.state.saveChanges(this.state.orders);
        this.props.navigation.goBack();
    }

    cancel() {
        this.props.navigation.goBack();
    }
}

const styles = StyleSheet.create({

    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#808B97' },
    text: { margin: 6 },
    row: { flexDirection: 'row', backgroundColor: '#FFF1C1' },
    btn: { width: 58, height: 18, backgroundColor: '#78B7BB', borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' },

    //container: {
    //    display: 'flex',
    //    flex: 1,
    //    flexDirection: 'column',
    //    justifyContent: 'flex-start',
    //    alignItems: 'flex-start'
    //},
    formRow: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    formButtom: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        paddingTop: 7,
        justifyContent: 'center',
    },
    formButton: {
        width: '30%',
        fontSize: 12,
    },
    // Remove button
    listButton: {
        //flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#007aff',
        marginTop: 5,
        marginBottom: 5,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    listBtnText: {
        alignSelf: 'center',
        color: '#007aff',
        //fontSize: 13,
        //fontWeight: '700',
        paddingTop: 13,
        paddingBottom: 13,
        //height: 17,
    },
    listTextBlock: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default OrderListScreen;
