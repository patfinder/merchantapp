﻿
export function formatVnd(val) {
    if (!val) return val;

    var valStr = Math.floor(val).toString();
    if (valStr.length % 3 !== 0) valStr = ' '.repeat(3 - (valStr.length % 3)) + valStr;

    return valStr.match(/.{3}/g).join('.');
}
