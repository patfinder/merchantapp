﻿import React from 'react';
import {
    View, ScrollView,
    ActivityIndicator, TextInput, Alert, Image, Text, Picker, TouchableOpacity
} from 'react-native';
import DatePicker from 'react-native-datepicker'
import { Input } from 'react-native-elements';
import moment from 'moment';
import * as _camelcase from 'camelcase';
var camelCase = _camelcase.default;

//import Icon from 'react-native-vector-icons/Feather';
import i18n from '../../i18n';
import SETTINGS from '../../settings';
import { Button } from '../../components/common';
import { toCapitalizedObject } from '../../libs/langUtils';
import { formatVnd } from './Utils';

const TRANS_INFO_TYPE = {
    PICK: 0,
    ENTER: 1,
};

const today = Date.now();

class OrderScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            // Create Order info

            // Summary
            thisMonthOrderCount: 0,
            thisMonthOrderAmount: 0.0,
            inventoryList: [],
            // [{custNo, plateNo, transportName, driverName, mobile}]
            transports: [],
            products: [],
            //[
            //    {
            //        productNo, // group-by: productNo, packingType
            //        [{ productNo, packingType, unitPrice, custNo, fDate, tDate, percent, discount }]
            //    }
            //]
            bigKinds: [],       // Select big-kind, then select product of big-kind
            productGroups: [],  // Mỗi product group là 1 nhóm của các đóng gói khác nhau của 1 SP 
            packingTypes: [],   // Packing type có sẵn cho SP đã chọn
            transInfoType: TRANS_INFO_TYPE.PICK,
            plateNo: undefined, // selected vehicle from list

            // General info
            inventoryNo: undefined,
            collectDate: undefined,

            // New Transport
            transportInfo: {
                driverName: undefined,
                plateNo: undefined,
                transportName: undefined,
                mobile: undefined,
                trailerPlate: undefined, // ??
            },

            // New Order
            order: {
                bigKind: undefined,
                productNo: undefined,
                packingType: undefined,
                quantity: undefined,
            },

            // Order List (of above shape)
            orders: [],

            loading: true,
        };

        this.setDebugValues = this.setDebugValues.bind(this);
        this.setTransportInfo = this.setTransportInfo.bind(this);
        this.setOrder = this.setOrder.bind(this);
        this.loadData = this.loadData.bind(this);
        this.addToList = this.addToList.bind(this);
        this.viewList = this.viewList.bind(this);
        this.saveListChanges = this.saveListChanges.bind(this);
        this.save = this.save.bind(this);
    }

    componentDidMount() {
        this.setState({ collectDate: moment(today).format(SETTINGS.DATE_FORMAT) });
        this.loadData();
    }

    render() {

        // General info
        var {
            loading, bigKinds, productGroups, packingTypes, transports,
            thisMonthOrderCount, thisMonthOrderAmount,
            //products
        } = this.state;

        // Inventory
        var { inventoryList, inventoryNo } = this.state;

        // Transport Info
        var { transInfoType, transportInfo, plateNo, collectDate } = this.state;
        var { driverName, tPlateNo, /*mobile, transportName, trailerPlate */} = transportInfo;

        // Order
        var { order, orders } = this.state;
        var { bigKind, productNo, packingType, quantity } = order;

        // Product matching selected Big Kind
        productGroups = bigKind && productGroups.filter(g => g.products[0].p1 === bigKind) || [];

        // selected product, with packing
        var product = productNo && productGroups.find(g => g.productNo === productNo) ?.products
            .find(p => p.packingType === packingType) || {};

        // Product
        var { unitPrice, percent, discount } = product;
        var afterDiscount = formatVnd(unitPrice - (unitPrice * percent) - discount + 0.5);
        var priceStr = unitPrice && `Giá: ${unitPrice} - Giảm giá: ${Math.floor(percent * 100)}% - ${discount}Đ = ${afterDiscount}Đ` || '';

        return (
            <View style={styles.container}>
                {
                    //<View style={{ height: 70, width: '100%', alignItems: 'center' }}>
                    //    <Image source={require('../../../resource/ant2.png')} />
                    //</View>
                }
                <View style={styles.formRow}>
                    <Text style={styles.summaryText}>{`Số lượng đơn hàng trong tháng ${thisMonthOrderCount}`}</Text>
                </View>
                <View style={styles.formRow}>
                    <Text style={styles.summaryText}>{`Sản lượng trong tháng ${thisMonthOrderAmount}`}</Text>
                </View>

                <ScrollView style={{width: '100%'}}>


                    {/* Order Info */}
                    <View style={styles.formRow}>
                        <Text style={{ fontWeight: 'bold' }}>Thông tin vận tải</Text>
                    </View>

                    {/* Collect Date */}
                    <View style={styles.formRow} style={{ paddingTop: 5 }}>
                        <DatePicker
                            style={{ width: 200 }}
                            minDate={moment(today).format(SETTINGS.DATE_FORMAT)}
                            date={collectDate}
                            mode="date" placeholder={i18n.t('order.collectDate')} format={SETTINGS.DATE_FORMAT}
                            confirmBtnText="Chọn"
                            cancelBtnText="Hủy bỏ" customStyles={{
                                dateIcon: {
                                    position: 'absolute', left: 0, top: 4, marginLeft: 0
                                },
                                dateInput: { marginLeft: 36 }
                            }}
                            onDateChange={collectDate => { this.setState({ collectDate }) }}
                        />
                    </View>

                    {/* Inventory List */}
                    <View style={styles.formRow}>
                        <Picker
                            selectedValue={inventoryNo}
                            style={styles.input}
                            onValueChange={inventoryNo => this.setState({ inventoryNo })}>

                            <Picker.Item label={i18n.t('order.inventory_label')} />
                            {inventoryList.map(inv => (
                                <Picker.Item key={inv.inventoryNo} label={inv.inventoryName} value={inv.inventoryNo} />
                            ))}
                        </Picker>
                    </View>

                    {/* Transport Info */}
                    <View style={styles.formRow} onTouchEnd={() => this.setState({ transInfoType: TRANS_INFO_TYPE.PICK })}>
                        <TouchableOpacity style={styles.circle}>
                            {transInfoType === TRANS_INFO_TYPE.PICK && (<View style={styles.checkedCircle} />)}
                        </TouchableOpacity>
                        <Text> Lấy thông tin từ danh sách xe</Text>
                    </View>
                    <View style={styles.formRow} onTouchEnd={() => this.setState({ transInfoType: TRANS_INFO_TYPE.ENTER })}>
                        <TouchableOpacity style={styles.circle}>
                            {transInfoType === TRANS_INFO_TYPE.ENTER && (<View style={styles.checkedCircle} />)}
                        </TouchableOpacity>
                        <Text> Nhập thông tin</Text>
                    </View>

                    { /*Verhicles List*/ }
                    {transInfoType === TRANS_INFO_TYPE.PICK && (
                        <Picker
                            selectedValue={plateNo}
                            style={styles.input}
                            onValueChange={plateNo => this.setState({ plateNo })}
                        >

                            <Picker.Item label={i18n.t('order.vehicle')} />
                            {transports.map(trans => (
                                <Picker.Item
                                    key={trans.plateNo}
                                    label={`${trans.driverName} - ${trans.plateNo}`}
                                    value={trans.plateNo}
                                />
                            ))}
                        </Picker>
                    )}

                    {/* Transport Fields */}
                    {transInfoType === TRANS_INFO_TYPE.ENTER && (<>
                        <View style={styles.formRow}>
                            <Input
                                placeholder={i18n.t('order.driverName')}
                                value={driverName}
                                onChangeText={driverName => this.setTransportInfo({ driverName })}
                                maxLength={20}
                                leftIcon={{ type: 'font-awesome', name: 'user' }}
                            />
                        </View>
                        <View style={styles.formRow}>
                            <Input
                                placeholder={i18n.t('order.plateNo')}
                                value={tPlateNo}
                                onChangeText={plateNo => this.setTransportInfo({ plateNo })}
                                maxLength={10}
                                leftIcon={{ type: 'font-awesome', name: 'truck' }}
                            />
                        </View>
                    </>)}

                    {/* Product P1 / Big Kind */}
                    <View style={styles.formRow}>
                        <Input
                            secureTextEntry
                            inputComponent={Picker}
                            selectedValue={bigKind}
                            onValueChange={bigKind => this.setOrder({ bigKind })}
                            leftIcon={{ type: 'font-awesome', name: 'shopping-bag' }}
                        >
                            <Picker.Item label={i18n.t('order.product_p1')} />
                            {bigKinds.map(bigKind => {
                                return (<Picker.Item key={bigKind.code} label={bigKind.vName} value={bigKind.code} />);
                            })}
                        </Input>
                    </View>

                    {/* Product */}
                    <View style={styles.formRow}>
                        <Input
                            secureTextEntry
                            inputComponent={Picker}
                            selectedValue={productNo}
                            onValueChange={productNo => this.setOrder({ productNo })}
                            leftIcon={{ type: 'font-awesome', name: 'shopping-bag' }}
                        >
                            <Picker.Item label={i18n.t('order.product')} />
                            {productGroups.map(pgroup => {
                                var label = pgroup.products[0].productVName ?
                                    `${pgroup.productNo} - ${pgroup.products[0].productVName}` :
                                    pgroup.productNo;
                                return (<Picker.Item key={pgroup.productNo} label={label} value={pgroup.productNo} />);
                            })}
                        </Input>
                    </View>

                    {/* Packing Type */}
                    <View style={styles.formRow}>
                        <Input
                            secureTextEntry
                            inputComponent={Picker}
                            selectedValue={packingType}
                            onValueChange={packingType => this.setOrder({ packingType })}
                            leftIcon={{ type: 'material-community', name: 'package-variant-closed' }}
                        >
                            <Picker.Item label={i18n.t('order.packingType')} />
                            {packingTypes.map(packing =>
                                <Picker.Item key={packing} label={`${packing} kg`} value={packing} />
                            )}
                        </Input>
                    </View>

                    {/* Price & Discount */}
                    <View style={styles.formRow}>
                        <Text>{priceStr}</Text>
                    </View>

                    {/* Quantity */}
                    <View style={styles.formRow}>
                        <Input
                            placeholder={i18n.t('order.quantity')}
                            inputComponent={TextInput}
                            keyboardType='numeric'
                            value={quantity?.toString()}
                            onChangeText={quantity => this.setOrder({ quantity: parseInt(quantity) })}
                            leftIcon={{ type: 'material-community', name: 'counter' }}
                        />
                    </View>
                </ScrollView>

                {/* Order Summary Info */}
                <View style={styles.formRow} /*style={{ justifyContent: 'flex-start' }}*/>
                    <Text style={styles.summaryText}>Số SP đã thêm: {orders.length}</Text>
                </View>

                {/* Loading */}
                {loading &&
                    <View style={styles.loading} pointerEvents='none' >
                        <ActivityIndicator size='large' />
                    </View>
                }

                {/* Form Buttons */}
                <View style={styles.formButtom}>
                    <View style={styles.formButton}>
                        <Button onPress={this.addToList}>Thêm SP</Button>
                    </View>
                    <View style={styles.formButton} style={{ width: '40%' }}>
                        <Button onPress={this.viewList}>Xem D.Sách</Button>
                    </View>
                    <View style={styles.formButton}>
                        <Button onPress={this.save} disabled={loading}>Đặt hàng</Button>
                    </View>
                </View>
            </View>
        );
    }

    setTransportInfo(transportInfo) {
        this.setState({ transportInfo: { ...this.state.transportInfo, ...transportInfo } });
    }

    setOrder(order) {
        // Select product, filter packing type
        if (order.productNo) {
            var productGrp = this.state.productGroups.find(g => g.productNo === order.productNo);
            var packingTypes = [...new Set(productGrp.products.map(p => p.packingType))];
            this.setState({ packingTypes, order: { ...this.state.order, ...order, packingType: undefined } });
        }
        else this.setState({ order: { ...this.state.order, ...order } });
    }

    /**
     * Debug only. Set values for skipping input.
     * */
    setDebugValues() {
        this.setState({
            //collectDate: moment(today).format(SETTINGS.DATE_FORMAT),
            //transportInfo: { plateNo: '50A 12.001' },
            orders: [{ bigKind: '04', productNo: 'V801', packingType: 25, quantity: 2 }],
        });
    }

    addToList() {
        var { order: { productNo, packingType, quantity } } = this.state;
        var errors = [];

        if (!productNo || !packingType || !quantity) {
            errors.push('Mời bạn chọn Sản phẩm, Loại đóng gói và Số lượng');
        }

        if (errors.length) {
            Alert.alert("Lỗi", errors.map(e => `${String.fromCharCode(0x2022)} ${e}`).join('\n'));
            return;
        }

        var { orders } = this.state;

        this.setState({ order: {}, orders: [...orders, {productNo, packingType, quantity}] });

        // TOD_: DEBUG ONLY, keep old values
        //this.setState({ orders: [...orders, { productNo, packingType, quantity }] });
    }

    viewList() {
        var orders = [...this.state.orders];
        var { products, productGroups } = this.state;

        // Add Name
        orders.forEach(order => {
            order.productVName = products.find(p => p.productNo === order.productNo).productVName;
        });
        this.props.navigation.navigate('OrderList', { orders, productGroups, saveChanges: this.saveListChanges });
    }

    saveListChanges(orders) {
        this.setState({ orders });
    }

    loadData() {

        this.setState({ loading: true });

        var url = `${SETTINGS.API.API_ROOT}${SETTINGS.API.ORDER.GET_CREATE_ORDER_INFO}`;
        //var url = `${SETTINGS.API.API_ROOT}${SETTINGS.API.PRODUCT.GET}`;
        //var url = `${SETTINGS.API.API_ROOT}auth/TestAnonymous`;

        fetch(url, {
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Cache': 'no-cache'
            },
        })
            .then(resp => {
                return Promise.all([resp, resp.ok && resp.json()])
            })
            .then(([resp, json]) => {
                console.log('OrderScreen - GET_CREATE_ORDER_INFO', json);

                // Auth error
                if ([401, 403].indexOf(resp.status) >= 0) {
                    Alert.alert('Thông báo', 'Phiên đang nhập hết hạn. Mời bạn đăng nhập lại.');
                    this.props.navigation.navigate('Login');
                    return;
                }

                if (!resp.ok) {
                    Alert.alert('Thông báo', 'Lỗi lấy dữ liệu sản phẩm');
                    return;
                }

                var { inventoryList, bigKinds, products, transports, thisMonthOrderCount, thisMonthOrderAmount } = json.data;

                // Product Groups: [{productNo, products}]
                var productGroups = products.reduce((acc, cur) => {
                    let group = acc.find(a => a.productNo === cur.productNo);

                    if (group) group.products.push(cur);
                    else {
                        group = { productNo: cur.productNo, products: [cur] };
                        acc.push(group);
                    }
                    return acc;
                }, []);

                var inventoryNo = inventoryList.length ? inventoryList[0].inventoryNo : undefined;
                this.setState({ inventoryList, bigKinds, products, productGroups, transports, inventoryNo, thisMonthOrderCount, thisMonthOrderAmount });

                // TOD_: DEBUG
                //this.setDebugValues();
            })
            .catch(error => {
                Alert.alert('Error', error.message);
            })
            .finally(() => this.setState({ loading: false }));
    }

    save() {
        var { inventoryNo, transInfoType, transportInfo, plateNo } = this.state;
        var { collectDate, orders } = this.state;

        var errors = [];

        if (transInfoType === TRANS_INFO_TYPE.PICK) {
            transportInfo = {};

            if (!plateNo) errors.push('Mời bạn chọn Phương tiện vận chuyển');
        }
        else if (transInfoType === TRANS_INFO_TYPE.ENTER) {
            plateNo = undefined;
        }

        if (!inventoryNo)   errors.push('Mời bạn chọn Kho lấy hàng');
        if (!collectDate)   errors.push('Mời bạn chọn Ngày lấy hàng');
        if (!orders.length) errors.push('Bạn phải Thêm ít nhất một sản phẩm');

        if (errors.length) {
            Alert.alert('Lỗi', errors.map(e => `${String.fromCharCode(0x2022)} ${e}`).join('\n'));
            return;
        }

        // Show confirmation
        Alert.alert('Xác nhận', 'Bạn có muốn gởi Phiếu đặt hàng', [
            {
                text: 'Gởi đi',
                onPress: proceedSave.bind(this),
                style: 'ok',
            },
            {
                text: 'Hủy bỏ',
                style: 'cancel',
            },
        ]);

        /**
         * Process handling save order.
         * */
        function proceedSave() {
            var orderMaster = {
                ...toCapitalizedObject({
                    placeNo: inventoryNo,
                    orderDate: moment(collectDate, SETTINGS.DATE_FORMAT),
                    plateNo,
                }),
                Transport: toCapitalizedObject(transportInfo),
                OrderDetails: orders.map(({ productNo, packingType, quantity: Qty }) => {
                    return toCapitalizedObject({ productNo, packingType, Qty });
                }),
            };
            var orderMasterStr = JSON.stringify(orderMaster);

            this.setState({ loading: true });

            var url = `${SETTINGS.API.API_ROOT}${SETTINGS.API.ORDER.CREATE}`;
            fetch(url, {
                method: 'POST',
                mode: 'cors',
                credentials: 'include',
                headers: { 'Content-Type': 'application/json' },
                body: orderMasterStr
            })
                .then(resp => {
                    return Promise.all([resp, resp.ok && resp.json()])

                    //var promise = new Promise((resolve, reject) => {
                    //    setTimeout(() => {
                    //        resolve(Promise.all([resp, resp.ok && resp.json()]));
                    //    }, 5000);
                    //});

                    //return promise;
                })
                .then(([resp, json]) => {
                    console.log('OrderScreen - CREATE json', [resp, json]);

                    // Auth error
                    if (Math.floor(resp.status / 100) === 4) {
                        Alert.alert('Thông báo', 'Phiên đang nhập hết hạn. Mời bạn đăng nhập lại.');
                        this.props.navigation.navigate('Login');
                        return;
                    }

                    if (resp.status === 200 && json.resultCode === 'Success') {
                        Alert.alert('Thông báo', 'Tạo Đơn hàng thành công.');
                        this.props.navigation.goBack();
                    }
                    else {
                        Alert.alert('Lỗi', json?.messages?.join('\n'));
                    }
                })
                .catch(error => {
                    Alert.alert('Error', error.message);
                })
                .finally(() => this.setState({ loading: false }));
        }
    }
}

const styles = {
    container: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    summaryText: {
        justifyContent: 'flex-start',
        textAlign: 'left',
        fontSize: 18,
    },
    formRow: {
        //flex: 1,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    formButtom: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        paddingTop: 7,
        justifyContent: 'center',
    },
    formButton: {
        width: '25%',
        fontSize: 12,
    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ACACAC',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkedCircle: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#794F9B',
    },
    input: {
        borderColor: '#CCCCCC',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 50,
        width: '90%',
        paddingLeft: 20,
        paddingRight: 20
    },
};

export default OrderScreen;
