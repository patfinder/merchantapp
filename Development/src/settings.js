
//const API_SERVER = '127.0.0.1:28205';
//const API_SERVER = '192.168.1.107:84';
//const API_SERVER = '10.9.21.199:84';

export default SETTINGS = {

    DATE_FORMAT: 'DD-MM-YYYY',

    API: {
        get API_ROOT() {
            // For emulator
            //API_ROOT: 'http://127.0.0.1:28205/',

            // Separated server
            //return 'http://115.74.202.122:81/';
            //return 'http://192.168.2.227:80/';      // must use port 80
            //return 'http://192.168.1.107:86/';
            //return 'http://192.168.1.107:28205/';

            //return 'http://10.9.21.199:84/';
            //return 'http://10.9.21.199:28205/';

            return global.loginInfo.serverAddress;
        },
        AUTH: {
            LOGIN: 'auth/login',
        },
        INVENTORY: {
            GET: 'inventory/get',
        },
        PRODUCT: {
            GET: 'product/get',
        },
        ORDER: {
            GET: 'order/Get',
            GET_CREATE_ORDER_INFO: 'order/GetCreateOrderInfo',
            CREATE: 'order/Create',
            GET_PREVIOUS_ORDERS: 'order/GetPreviousOrders',
        },
    }
};
