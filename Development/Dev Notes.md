================================
Nov 03, 2019

adb tcpip 5555
adb connect 192.168.2.229:5555
adb shell input keyevent 82 
doskey dmenu=adb shell input keyevent 82

rm -rf node_modules
npm i
npm start -- --reset-cache
Removing package lock file may also needed

Gradle/Maven - Behind the great Firewall
https://docs-as-co.de/news/enterprise-edition/

* Global config
<USER_HOME>/.gradle/gradle.properties

================================
Nov 09, 2019

Mismatching name in package.json & package-lonk.json may cause RN Android app error
(This may happen when renaming app name)

- Check / change Dev Server settings
	* Open RN App Dev Settings menu (press menu button on app)
	* Debugging - Debug Server host & port
	* Set value to point to dev PC IP : 8081 (default)
